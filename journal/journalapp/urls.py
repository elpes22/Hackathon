from django.urls import path
from journalapp.views import *

urlpatterns = [
    path('', attendance),
    path('groups/', group_list),
    path('courses/', course_list),
    path('cr_course/', create_course),
    path('tutors/', tutor_list),
    path('cr_group/', create_group),
    path('mark_attendancies/', mark_attendancies),
]
